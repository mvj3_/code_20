<?php

/**
 * 对前台输出的内容进行安全过滤
 * encode:输出dhtmlspecialchars编码后的文字(默认)
 * text:输出带纯文本，即过滤掉所有代码
 * safe:输出安全的html代码，如保留br，p等无安全风险的html代码
 * @param mixed $content
 * @param string $output
 */
function safe($content, $output = 'encode')
{
	switch ($output)
	{
		case 'encode':
			return dhtmlspecialchars($content);
		case 'text':
			return dhtmlspecialchars(strip_tags($content));
		case 'safe':
			return h($content);
		default:
			return dhtmlspecialchars($content);
	}
}